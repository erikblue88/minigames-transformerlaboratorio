﻿using System.Text.RegularExpressions;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class LoginControl : Singleton<LoginControl>
{
    [SerializeField] private TMP_InputField emailField;
    [SerializeField] private TextMeshProUGUI statusText;
    [SerializeField] private GameObject errorMessage;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public void Login()
    {
        if (validateEmail(emailField.text))
            Cloud.instance.Login(emailField.text);
        else
            SetMessage("Ingresa un correo válido", true);
    }

    public void Registro()
    {
        if (validateEmail(emailField.text))
            Cloud.instance.Registro(emailField.text);
        else
            SetMessage("Ingresa un correo válido", true);
    }

    public void SetMessage(string message, bool isError)
    {
        //statusText.color = isError ? Color.red : Color.green;
        errorMessage.SetActive(isError);
        //statusText.text = message;
    }

    public static bool validateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    public void OpenKeyBoard()
    {
        TouchScreenKeyboard.Open("",TouchScreenKeyboardType.EmailAddress);
    }
}
