﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderboardControl : Singleton<LeaderboardControl>
{
    [SerializeField] private List<TextMeshProUGUI> emails;
    [SerializeField] private List<TextMeshProUGUI> scores1;
    [SerializeField] private List<TextMeshProUGUI> scores2;
    [SerializeField] private List<TextMeshProUGUI> scores;
    [SerializeField] private GameObject panelLeaderboard;

    // Start is called before the first frame update
    public void OpenLeaderboard()
    {
        FindObjectOfType<AudioManager>().ButtonClickSound();
        Cloud.instance.DownloadLeaderboard();
        panelLeaderboard.SetActive(true);
    }

    public void CloseLeaderboard()
    {
        FindObjectOfType<AudioManager>().ButtonClickSound();
        panelLeaderboard.SetActive(false);
    }

    public void ArmaLeaderboard(ViewLeaderBoard leaderboard)
    {
        print(leaderboard.datos.tabla_posicion.Count);
        for(int x = 0; x < 10; x++)
        {
            if (x < leaderboard.datos.tabla_posicion.Count)
            {
                emails[x].text = leaderboard.datos.tabla_posicion[x].nombre;
                scores1[x].text = leaderboard.datos.tabla_posicion[x].score_juego_1;
                scores2[x].text = leaderboard.datos.tabla_posicion[x].score_juego_2;
                scores[x].text = leaderboard.datos.tabla_posicion[x].score;
            }
            else
                x = 10;
        }
    }
}
