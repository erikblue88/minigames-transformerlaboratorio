﻿[System.Serializable]
public class DatosUser
{
    public string id_usuario;
    public string token;
    public string email;
}

[System.Serializable]
public class ViewUser
{
    public bool exito;
    public string mensaje;
    public DatosUser datos;
}