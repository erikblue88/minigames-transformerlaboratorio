﻿using System.Collections.Generic;

[System.Serializable]
public class ViewMessage
{
    public bool exito;
    public string mensaje;
    public string datos;
}