﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

    public GameObject collisionParticle;

    private MeshFilter playerMeshFilter;
    private GameObject tempParticle;

	void Start () {
        playerMeshFilter = GetComponent<MeshFilter>();
	}
	
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CubeObst Instance") || other.CompareTag("PrismObst Instance") || other.CompareTag("SphereObst Instance"))     //If collides with an obstacle
        {
            if (other.CompareTag(playerMeshFilter.mesh.name))       //If the obstacle and the player have the same shape
            {
                tempParticle = Instantiate(collisionParticle, transform.position, Quaternion.identity);      //Istantiates a collision particle to the player's position
                tempParticle.GetComponent<ParticleSystemRenderer>().mesh = other.GetComponent<MeshFilter>().mesh;       //Sets particle's mesh identical to the obstacle's mesh
                Destroy(tempParticle, 3f);       //Destroys particle after x seconds
                FindObjectOfType<ScoreManager>().IncrementScore();      //Increments score
                FindObjectOfType<AudioManager>().ScoreSound();      //Plays scoreSound
                if(other.CompareTag("PrismObst Instance"))
                {
                    FindObjectOfType<ScoreManager>().IncrementToken();      //Increments token
                }
            }
            else     //If they have different shapes
            {
                GetComponent<Animation>().Play("PlayerDeathAnim");      //Plays playerDeathAnim
                FindObjectOfType<AudioManager>().DeathSound();      //Plays deathSound
                FindObjectOfType<GameManager>().EndPanelActivation();
            }
        }
    }
}
