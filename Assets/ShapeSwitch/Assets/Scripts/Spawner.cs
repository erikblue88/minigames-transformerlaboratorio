﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] obstacles, buttonPanels, players, grounds;
    public float timeBetweenSpawns, timeIncrease, spawnPositionX, spawn2After, spawn3After;

    private Vector3 spawnPos;
    private int countOfSpawns = 0;
    private bool spawnChanged = false;
    private Animation cameraAnim;

    public int spawnType = 0;       //Increases when the count of players increases too

	void Start () {
        cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>();
        spawnPos = transform.position;
        buttonPanels[0].SetActive(true);        //Only one button is available (for only one player)
        Spawn();        //First spawn
	}

    public void Spawn()
    {
        switch (spawnType)
        {
            case 0:
                Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                break;

            case 1:
                if (!spawnChanged)
                {
                    Invoke("PanelSwitch1", 1f);
                    cameraAnim.Play("CameraLeftAnim");
                    timeBetweenSpawns += timeIncrease;
                    players[1].SetActive(true);
                    grounds[1].SetActive(true);
                    spawnChanged = true;
                }
                else
                {
                    spawnPos = transform.position;
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                    spawnPos.x = 0f - spawnPositionX;
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                }
                break;

            case 2:
                if (!spawnChanged)
                {
                    Invoke("PanelSwitch2", 1f);
                    cameraAnim.Play("CameraRightAnim");
                    timeBetweenSpawns += timeIncrease * 1.4f;
                    players[2].SetActive(true);
                    grounds[2].SetActive(true);
                    spawnChanged = true;
                }
                else
                {
                    spawnPos = transform.position;
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                    spawnPos.x = 0f - spawnPositionX;
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                    spawnPos.x = 0f + spawnPositionX;
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)], spawnPos, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
                }
                break;
        }

        if (!(FindObjectOfType<GameManager>().gameIsOver))        //Invokes the next spawn only if the game is not over
            Invoke("Spawn", timeBetweenSpawns);     //Next spawn after 'timeBetweenSpawns' secs

        ChooseSpawnType();

        countOfSpawns++;
    }

    public void PanelSwitch1()
    {
        buttonPanels[0].SetActive(false);
        buttonPanels[1].SetActive(true);
    }

    public void PanelSwitch2()
    {
        buttonPanels[1].SetActive(false);
        buttonPanels[2].SetActive(true);
    }

    public void ChooseSpawnType()
    {
        if (countOfSpawns < spawn2After)
            spawnType = 0;
        else if ((countOfSpawns >= spawn2After) && (countOfSpawns < spawn3After))
        {
            spawnType = 1;
            if (countOfSpawns == spawn2After)
                spawnChanged = false;
        }
        else if (countOfSpawns >= spawn3After)
        {
            spawnType = 2;
            if (countOfSpawns == spawn3After)
                spawnChanged = false;
        }
    }
}
