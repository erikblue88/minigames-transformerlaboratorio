﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour {

    public TextMeshProUGUI scoreText, tokenText;
    public GameManager gm;

    private Animation tokenTextAnim;

    [HideInInspector]
    public int score = 0;
    public int correctas = 0;

    void Start()
    {
        tokenTextAnim = tokenText.gameObject.GetComponent<Animation>();     //Initializes tokenTextAnim
        tokenText.text = PlayerPrefs.GetInt("Token", 0).ToString();     //Writes out the number of tokens to the screen
    }

    public void IncrementScore()
    {
        if (FindObjectOfType<GameManager>().gameIsOver == false)       //If the game is not over
        {
            int curtokens = PlayerPrefs.GetInt("Token", 0);
            correctas++;
            if (curtokens == 0)
            {
                scoreText.text = (++score).ToString();      //Increments the 'scoretext' text as well as the score variable's value and writes it out to the screen
            }
            else
            {
                score += curtokens * 5;
                scoreText.text = score.ToString();
            }
            if(PreguntasControl.instance.tickets.Count > 0 && (correctas == 3 || 
                                                               correctas == 6 ||
                                                               correctas == 13 ||
                                                               correctas == 19 ||
                                                               correctas == 30))
            {
                gm.PanelPreguntaActive(true, 0.0f);
            }
            else if(PreguntasControl.instance.tickets.Count > 0 && correctas > 30 && (correctas - 30) % 9 == 0)
            {
                gm.PanelPreguntaActive(true, 0.0f);
            }
        }
    }

    public void IncrementScore(int bonus)
    {
        if (FindObjectOfType<GameManager>().gameIsOver == false)       //If the game is not over
        {
            int curtokens = PlayerPrefs.GetInt("Token", 0);
            //correctas++;
            if (curtokens == 0)
                scoreText.text = (++bonus).ToString();      //Increments the 'scoretext' text as well as the score variable's value and writes it out to the screen
            else
            {
                score += curtokens * 5 * bonus;
                scoreText.text = score.ToString();
            }
        }
        FindObjectOfType<AudioManager>().ScoreSound();      //Plays scoreSound
        //if (correctas % 3 == 0 && PreguntasControl.instance.tickets.Count > 0)
        //    gm.PanelPreguntaActive(true, 0.0f);
    }

    public void IncrementToken()
    {
        if (FindObjectOfType<GameManager>().gameIsOver == false)       //If the game is not over
        {
            PlayerPrefs.SetInt("Token", PlayerPrefs.GetInt("Token", 0) + 1);        //Increases the number of tokens
            tokenText.text = PlayerPrefs.GetInt("Token", 0).ToString();     //Writes out the number of tokens to the screen
            tokenTextAnim.Play();       //Plays tokenTextAnim
            FindObjectOfType<AudioManager>().TokenSound();      //Plays tokenSound
        }
    }
}
