﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeChange : MonoBehaviour {

    public Mesh[] playerMeshes;

    private MeshFilter playerMesh;
    private Animation playerAnim;

    [HideInInspector]
    public int tempMeshIndex = 0, countOfMeshes;

    void Start() {
        //Initialization
        playerMesh = GetComponent<MeshFilter>();
        playerAnim = GetComponent<Animation>();
        countOfMeshes = playerMeshes.Length;
    }

    public void ChangeShape()
    {
        if (tempMeshIndex + 1 < countOfMeshes)      //If the temporary mesh index would be smaller than the array's length if we increased it, then we switch to the next element of the array
            tempMeshIndex++;
        else     //If the temporary mesh index would be out of array if we increased it, then we switch back to the first element of the array
            tempMeshIndex = 0;
        PlayChangeAnim();       //Plays 'changeAnimation'
        Invoke("NewShape", 0.07f);
        FindObjectOfType<AudioManager>().ShapeChangeSound();
    }

    public void NewShape()
    {
        playerMesh.mesh = playerMeshes[tempMeshIndex];      //Changes the player's mesh
    }

    public void PlayChangeAnim()
    {
        //Deals with the rotation problem of the shapes based on the tempMeshIndex
        //If you change the order of the meshes in playerMeshes array, then the rotations will be messed up
        switch (tempMeshIndex)
        {
            case 0:
                playerAnim.Play("SphereToCubeAnim");
                break;
            case 1:
                playerAnim.Play("CubeToPrismAnim");
                break;
            case 2:
                playerAnim.Play("PrismToSphereAnim");
                break;
        }
    }
}
